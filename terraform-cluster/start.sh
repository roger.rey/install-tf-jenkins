#!/bin/bash

#directory /install-tf-jenkins/terraform-cluster

#DEPLOY CLUSTER KUBERNETES

terraform init

terraform apply -auto-approve

#Kubectl configuration

aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)

kubectl apply -f metrics-server-0.3.6/deploy/1.8+/

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml

kubectl apply -f https://raw.githubusercontent.com/hashicorp/learn-terraform-provision-eks-cluster/master/kubernetes-dashboard-admin.rbac.yaml

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep service-controller-token | awk '{print $1}')


#DEPLOY JENKINS SERVER

#Go to repository jenkins/helm-charts/charts/jenkins

helm install -f /jenkins/helm-charts/charts/jenkins/values.yaml my-jenkins jenkins/jenkins

#Adding cluster role

kubectl create clusterrolebinding permissive-binding --clusterrole=cluster-admin --user=admin --user=kubelet --group=system:serviceaccounts

#Adding Jenkins user to Jenkins

java -jar jenkins-cli.jar -auth admin:admin123456 -s http://localhost:8080/ create-credentials-by-xml system::system::jenkins _  < credential.xml

#Creating aws load-balancer

kubectl expose pod my-jenkins-0 --port=80 --target-port=8080 --name=jenkins-service --type=LoadBalancer

echo "This is your Jenkins url: "

kubectl get service jenkins-service

echo "Jenkins user: admin"
echo "Jenkins password: admin123456"
